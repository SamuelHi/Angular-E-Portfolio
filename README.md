# Angular E Portfolio

Overview of my E-Portfolio about Angular

## Notice
 
To try the Demo Version just clone the project and use the command "npm install" there 

## 1. Installation

First you need to install nodejs. To see if you already have a version installed type "node -v" in a CLI. 

If you don't have it you can get it here:
https://nodejs.org/en/download/ 

And you will need to install the Angular CLI.
Therefore just open a terminal window and use "npm install -g @angular/cli"

You also should install and IDE to work on the files. My recommendation is Visual Studio Code.

## 2. Creating a project

After everything is installed you can start with creating a project.

    1. Create a new folder where your Project will be in

    2. open a terminal in this directory and use the command "ng new <name>"

    3. everything will be created for you and you just have to wait till its finished, if you get asked y/N just hit enter, it will use the default settings

    4. To see if everything works go into the project folder (cd <name>)

    5. Use the command "ng serve --open" and an browserpage should open and show you this page
    
![success](app-works.png)




