import { Component, OnInit, Input } from '@angular/core';
import { Student } from '../student'

@Component({
  selector: 'app-studi-details',
  templateUrl: './studi-details.component.html',
  styleUrls: ['./studi-details.component.css']
})
export class StudiDetailsComponent implements OnInit {

  @Input() studi:Student;
  constructor() { }

  ngOnInit(): void {
  }

  getColor(note: number) {
    switch (note) {
      case 1:
        return "green"
        break;
      case 2:
        return "green"
        break;
      case 3:
        return "yellow"
        break;
      case 4:
        return "orange"
        break;
     
      default:
        return "red"
        break;
    }
  }

}
