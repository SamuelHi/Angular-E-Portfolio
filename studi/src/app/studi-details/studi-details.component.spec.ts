import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudiDetailsComponent } from './studi-details.component';

describe('StudiDetailsComponent', () => {
  let component: StudiDetailsComponent;
  let fixture: ComponentFixture<StudiDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudiDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
