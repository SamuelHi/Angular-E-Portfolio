import { TestBed } from '@angular/core/testing';

import { StudiServerService } from './studi-server.service';

describe('StudiServerService', () => {
  let service: StudiServerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudiServerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
