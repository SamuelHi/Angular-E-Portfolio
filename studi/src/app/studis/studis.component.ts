import { Component, OnInit } from '@angular/core';
import {Studenten} from '../mock-students'
import {Student} from '../student'
import {StudiServerService}  from '../studi-server.service'
@Component({
  selector: 'app-studis',
  templateUrl: './studis.component.html',
  styleUrls: ['./studis.component.css']
})
export class StudisComponent implements OnInit {

  studenten:Student[] = [];

  selectedStudi;

  OnSelect(chosenStudi){

    this.selectedStudi = chosenStudi;
  }


  constructor(private studiservice:StudiServerService) { }

  ngOnInit(): void {
    this.getStudies();
  }

  getStudies():void{
    this.studiservice.getStudent().subscribe(studis => this.studenten = studis)
  }
}
