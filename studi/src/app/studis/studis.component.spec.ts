import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudisComponent } from './studis.component';

describe('StudisComponent', () => {
  let component: StudisComponent;
  let fixture: ComponentFixture<StudisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
