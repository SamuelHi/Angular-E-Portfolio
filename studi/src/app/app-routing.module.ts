import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudisComponent } from './studis/studis.component'
import { HomeComponent } from './home/home.component'

const routes: Routes = [
  { path: 'studis', component: StudisComponent },
  { path: 'home', component:HomeComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
