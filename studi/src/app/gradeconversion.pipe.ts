import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gradeconversion'
})
export class GradeconversionPipe implements PipeTransform {

  transform(value: number): string {
    switch (value) {
        case 1:
            return "A"
            break;
        case 2:
            return "B"
            break;
        case 3:
            return "C"
            break;
        case 4:
            return "D"
            break;
        case 5:
            return "E"
            break;
        case 6:
            return "F"
            break;

    }
  }

}
