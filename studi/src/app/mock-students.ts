import {Student} from '../app/student'

export const Studenten: Student[] = [
    { matrikelnummer: 1230,name: 'Kochtopf', studiengang:'Maschinenbau',noten:[1,2,3]},
    { matrikelnummer: 1231,name: 'Habermaß', studiengang:'Informatik',noten:[3,2,1]},
    { matrikelnummer: 1232,name: 'Rederei', studiengang:'Informatik',noten:[4,3,2]},
    { matrikelnummer: 1233,name: 'Gainz', studiengang:'Mathe',noten:[2,2,2]},
    { matrikelnummer: 1234,name: 'S.L.Jackson', studiengang:'Theologie',noten:[1,1,5]},

  ];
