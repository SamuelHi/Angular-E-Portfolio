export interface Student {
    matrikelnummer: number;
    name: string;
    studiengang:string;
    noten:number[];
  }
  