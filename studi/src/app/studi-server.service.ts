import { Injectable } from '@angular/core';
import { Student } from './student'
import {Studenten} from './mock-students'
import {Observable, of} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class StudiServerService {

  getStudent():Observable<Student[]>{
    const studis = of(Studenten);
    return studis;
  }
  constructor() { }
}
